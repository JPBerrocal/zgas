﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using Microsoft.Xrm.Sdk;
using System.Data;
using Microsoft.Xrm.Sdk.Query;
using ZPremia_PedidoIntegracion;


namespace ZGas_Retransmisiones
{
    class DBConnect
    {
        private MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string password;
        private int port;


        //Constructor
        public DBConnect()
        {
            server = "JPBR-LAPTOP";
            database = "zgas";
            uid = "JP";
            password = "a123456!";
            
            Initialize(server, database, uid, password, port);
        }

        public DBConnect(String server, String database, String user, String password, int port)
        {
            Initialize(server, database, user, password, port);
        }

        //Initialize values
        private void Initialize(String server, String database, String user, String password, int port)
        {
            this.server = server;
            this.database = database;
            this.uid = user;
            this.password = password;
            this.port = port;
            string connectionString = "SERVER=" + this.server + ";" + "DATABASE="
                +  this.database + ";" + "UID=" + this.uid + ";" + "PASSWORD=" + this.password + ";";

            connection = new MySqlConnection(connectionString);
        }

        
        private bool OpenConnection()
        {
            
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                        throw new InvalidPluginExecutionException("No se pudo establecer la conexion a la base de datos de ZPremia: " + ex.Message);
                        break;

                    case 1045:
                        throw new InvalidPluginExecutionException("Usuario/password de Zpremia invalido");
                        break;
                }
                return false;
            }
         
        }

        //Close connection
        private bool CloseConnection()
        {
            
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                throw new InvalidPluginExecutionException("Error al cerrar la conexion a la base de datos ZPremia" + ex.Message);
            }
           
        }
        
        

        public String insertarPedido(String numcef, int interlocutor, String nombre
            , String ptipoCilindro, String mensaje, DateTime fecha, String pstatus, String ptransmicion, String pdireccion, Guid paisID, IOrganizationService service)
        {
            bool insercion = true;
            int contador = 0;

            while (insercion) {
                #region buscar el consecutivo para el numero de servicio

                ConditionExpression condition_one = new ConditionExpression();
                condition_one.AttributeName = "new_name";
                condition_one.Operator = ConditionOperator.Equal;
                condition_one.Values.Add("PedidoExpress");


                ConditionExpression condition_two = new ConditionExpression();
                condition_two.AttributeName = "new_paisid";
                condition_two.Operator = ConditionOperator.Equal;
                condition_two.Values.Add(paisID);

                FilterExpression filter_one = new FilterExpression();
                filter_one.Conditions.Add(condition_one);
                filter_one.Conditions.Add(condition_two);

                QueryExpression query_one = new QueryExpression
                {
                    EntityName = new_consecutivos.EntityLogicalName,
                    ColumnSet = new ColumnSet("new_contador"),
                    Criteria = filter_one
                };


                DataCollection<Entity> collection = service.RetrieveMultiple(query_one).Entities;

                if (collection.Count > 0)
                {
                    Entity consecutivo = (Entity)collection[0];
                    contador = (int)consecutivo["new_contador"] + 1;
                    consecutivo["new_contador"] = contador;
                    service.Update(consecutivo);
                }
                else
                {
                    throw new InvalidPluginExecutionException("No se pudo encontrar el valor del consecutivo");
                }
                #endregion


                if (this.OpenConnection() == true)
                {
                    try
                    {
                        MySqlCommand cmd = new MySqlCommand("usp_InsertInterfase_CRM", connection);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new MySqlParameter("pnumservicio", contador));
                        cmd.Parameters.Add(new MySqlParameter("pnumcef", numcef));
                        cmd.Parameters.Add(new MySqlParameter("pinterlocutor", interlocutor));
                        cmd.Parameters.Add(new MySqlParameter("pnombre", nombre));
                        cmd.Parameters.Add(new MySqlParameter("ptipoCilindro", ptipoCilindro));
                        cmd.Parameters.Add(new MySqlParameter("pmensaje", mensaje));
                        cmd.Parameters.Add(new MySqlParameter("pfecha", fecha));
                        cmd.Parameters.Add(new MySqlParameter("pstatus", pstatus));
                        cmd.Parameters.Add(new MySqlParameter("ptransmicion", ptransmicion));
                        cmd.Parameters.Add(new MySqlParameter("pdireccion", pdireccion));
                        //cmd.Connection.Open();
                        cmd.ExecuteNonQuery();
                        cmd.Connection.Close();

                        this.CloseConnection();
                        insercion = false;
                    }
                    catch (MySqlException me)
                    {
                       
                        if ((me.Number == 1062))
                        {
                            insercion = true;
                            this.CloseConnection();
                        }
                        else
                        {
                            throw new InvalidPluginExecutionException("Error insertando en ZPremia: " + me.Message + " Numero: " + me.Number);
                        }
                    }
                    
                }
            }//while
            
           
            return contador.ToString();
        }

        public bool actualizarPedido(String numServicio, String numcef, int interlocutor, String nombre
            , String ptipoCilindro, String mensaje, DateTime fecha, String pstatus
            , String ptransmicion, String pdireccion)
        {
            bool result = true;

            if (this.OpenConnection() == true)
            {
                try
                {
                    MySqlCommand cmd = new MySqlCommand("usp_UpdateInterfase_CRM", connection);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new MySqlParameter("pnumservicio", numServicio));
                    cmd.Parameters.Add(new MySqlParameter("pnumcef", numcef));
                    cmd.Parameters.Add(new MySqlParameter("pinterlocutor", interlocutor));
                    cmd.Parameters.Add(new MySqlParameter("pnombre", nombre));
                    cmd.Parameters.Add(new MySqlParameter("ptipoCilindro", ptipoCilindro));
                    cmd.Parameters.Add(new MySqlParameter("pmensaje", mensaje));
                    cmd.Parameters.Add(new MySqlParameter("pfecha", fecha));
                    cmd.Parameters.Add(new MySqlParameter("pstatus", pstatus));
                    cmd.Parameters.Add(new MySqlParameter("ptransmicion", ptransmicion));
                    cmd.Parameters.Add(new MySqlParameter("pdireccion", pdireccion));
					//cmd.Parameters.Add(new MySqlParameter("ptipoPos", tipoPos));
					//cmd.Parameters.Add(new MySqlParameter("ppuntos", puntos));
					//cmd.Connection.Open();
					cmd.ExecuteNonQuery();
                    cmd.Connection.Close();

                    this.CloseConnection();
                }
                catch (MySqlException me)
                {
                    result = false;
                    this.CloseConnection();
                    throw new InvalidPluginExecutionException("Error retransmitiendo pedido a ZPremia: " + me.Message + " Numero: " + me.Number);
                }
            }
            return result;
        }
       

        
    
    }//~
}////
