﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZGas_Retransmisiones
{

    /// <summary>
    /// Esta clase se utiliza para armar el string correspondiente al tipo de cilindro, llave cantidad de cilindros, recibe en el constructor todos los
    /// parametros necesarios para armar el string esto es, el tipo de cilindro y la cantidad de cada una de las lineas del pedido
    /// </summary>
    class TipoCilindro
    {

        private String codigo = "";

        public TipoCilindro(List<CilindroOV> lista)
        {
            foreach (CilindroOV c in lista)
            {
                formarCodigo(c);
            }
        }

        private void formarCodigo(CilindroOV cilindro)
        {
            String cantidad = cilindro.Cantidad.ToString("D2");
            String tipoLlave = cilindro.TipoLlave;

            StringBuilder stbuilder = new StringBuilder(codigo);
            stbuilder.Append(tipoLlave);
            stbuilder.Append("(");
            stbuilder.Append(cantidad);
            stbuilder.Append(")");
            codigo = stbuilder.ToString();
        }


        public String CodigoProductoCantidad
        {
            get { return codigo; }
        }

        
        
        private enum TipoLLave
        {
            //25 libras llave Dgas
            VD = 100000000,
            //25 libras llave Z
            VZ = 100000001,
            //35 libras llave Z
            TZ = 100000002,
            //35 libras llave Dgas
            TD = 100000003,
            //35 libras llave Shelani
            TS = 100000004,
            //100 libras llave Shelani
            CS = 100000005,
            //100 libras llave Pol
            CP = 100000006
        };

    }//class
}//namespace
