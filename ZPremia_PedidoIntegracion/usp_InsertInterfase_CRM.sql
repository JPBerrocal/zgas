DELIMITER $$;

DROP PROCEDURE IF EXISTS `zgas`.`usp_InsertInterfaseCRM_Tarjeta`$$

CREATE DEFINER=`r3`@`%` PROCEDURE `usp_InsertInterfaseCRM_Tarjeta`(pnumservicio int, pnumcef varchar(10)
	, pinterlocutor int, pnombre varchar(80), ptipoCilindro varchar(126), pmensaje text
	, pfecha date, pstatus varchar(10), ptransmicion char(1), pdireccion varchar(150), ptipoPos varchar(8), ppuntos varchar(10), psectorRecuperacion varchar(100))
BEGIN
INSERT INTO interfase_crm (
		pedido, 
		CEF, 
		cliente, 
		nombre, 
		tipo_cilindro, 
		mensage, 
		fecha, 
		status, 
		transmision, 
		direccion, 
		tarjeta,
		TIPOPOS,
		puntos,
		SectorRecuperacion) 
	VALUES 
	(
		pnumservicio, 
		pnumcef, 
		pinterlocutor, 
		pnombre, 
		ptipoCilindro, 
		pmensaje, 
		pfecha, 
		pstatus, 
		ptransmicion, 
		pdireccion, 
		'SI',
		ptipoPos,
		ppuntos,
		psectorRecuperacion
	);
END$$

DELIMITER ;$$

////////////////////////////////
DELIMITER $$;

DROP PROCEDURE IF EXISTS `zgas`.`usp_InsertInterfase_CRM`$$

CREATE DEFINER=`root`@`%` PROCEDURE `usp_InsertInterfase_CRM`(pnumservicio int, pnumcef varchar(10)
	, pinterlocutor int, pnombre varchar(80), ptipoCilindro varchar(126), pmensaje text
	, pfecha date, pstatus varchar(10), ptransmicion char(1), pdireccion varchar(150)
	, ptelefono varchar(25), poperador varchar(100), pmunicipio varchar(200), pdepartamento varchar(200)
	, ptipoPos varchar(8), ppuntos varchar(10), psectorRecuperacion varchar(100))
BEGIN
	INSERT INTO interfase_crm (pedido, CEF, cliente, nombre, tipo_cilindro, mensage, fecha, status, transmision, direccion, telefono, operador, municipio, departamento, TIPOPOS, puntos, SectorRecuperacion  ) VALUES (pnumservicio, pnumcef, pinterlocutor, pnombre
	, ptipoCilindro, pmensaje, pfecha, pstatus, ptransmicion, pdireccion, ptelefono, poperador, pmunicipio, pdepartamento, ptipoPos, ppuntos, psectorRecuperacion);
END$$

DELIMITER ;$$

///////////////////////
DELIMITER $$;

DROP PROCEDURE IF EXISTS `zgas`.`usp_UpdateInterfase_CRM`$$

CREATE DEFINER=`root`@`%` PROCEDURE `usp_UpdateInterfase_CRM`(pnumservicio int, pnumcef varchar(10)
	, pinterlocutor int, pnombre varchar(80), ptipoCilindro varchar(126), pmensaje text
	, pfecha date, pstatus varchar(10), ptransmicion char(1), pdireccion varchar(150))
BEGIN
	
	UPDATE MENSAJESCRM 
		SET CEF = pnumcef, 
			cliente = pinterlocutor, 
			nombre = pnombre, 
			tipo_cilindro = ptipoCilindro, 
			mensage = pmensaje, 
			fecha = pfecha, 
			status = pstatus, 
			transmision = ptransmicion, 
			direccion = pdireccion
	WHERE pedido = pnumservicio;
END$$

DELIMITER ;$$
