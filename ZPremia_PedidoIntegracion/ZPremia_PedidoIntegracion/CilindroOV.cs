﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZPremia_PedidoIntegracion
{
    
    /// <summary>
    /// Clase objeto valor para almacenar la cantidad y tipo de llave del cilindro
    /// </summary>
    class CilindroOV
    {

        private int cantidad;
        private string tipoLlave;


        public CilindroOV(int cantidad, string tipoLlave)
        {
            this.cantidad = cantidad;
            this.tipoLlave = tipoLlave;
        }


        public string TipoLlave
        {
            get { return tipoLlave; }
        }


        public int Cantidad
        {
            get { return cantidad; }
        }

    }//
}
