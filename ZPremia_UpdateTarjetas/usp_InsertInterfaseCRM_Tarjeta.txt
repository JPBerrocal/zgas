DELIMITER $$;

DROP PROCEDURE IF EXISTS `zgas`.`usp_InsertInterfaseCRM_Tarjeta`$$

CREATE DEFINER=`r3`@`%` PROCEDURE `usp_InsertInterfaseCRM_Tarjeta`(pnumservicio int, pnumcef varchar(10)
	, pinterlocutor int, pnombre varchar(80), ptipoCilindro varchar(126), pmensaje text
	, pfecha date, pstatus varchar(10), ptransmicion char(1), pdireccion varchar(150), ptipoPos varchar(8), ppuntos varchar(10))
BEGIN
INSERT INTO interfase_crm (
		pedido, 
		CEF, 
		cliente, 
		nombre, 
		tipo_cilindro, 
		mensage, 
		fecha, 
		status, 
		transmision, 
		direccion, 
		tarjeta,
		TIPOPOS,
		puntos) 
	VALUES 
	(
		pnumservicio, 
		pnumcef, 
		pinterlocutor, 
		pnombre, 
		ptipoCilindro, 
		pmensaje, 
		pfecha, 
		pstatus, 
		ptransmicion, 
		pdireccion, 
		'SI',
		ptipoPos,
		ppuntos
	);
END$$

DELIMITER ;$$